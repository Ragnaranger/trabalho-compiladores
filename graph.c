#include "graph.h"
#include "stdlista.h"


#include <stdio.h>
#include <stdbool.h>

struct Vertice{
  int id;
  int degree;
  bool active;
  ListStart edges; // List of vertices
  int color;
};

struct Graph{
  int number_of_vertices;
  ListStart vertices; // List of vertices
};

struct Argument{
  struct Vertice* vertice;
  int k;
  int k_inicial;
};

/**
 *  Se print_arestas == True, chama a função para as arestas do grafo
 */
void print_vertice(void *_vertice, void *_print_arestas){
  struct Vertice *vertice = (struct Vertice *) _vertice;
  int *print_arestas = _print_arestas;

  if (*print_arestas == 1){
    printf("%d - Vertice %2d ==> ", vertice->active, vertice->id);
    *print_arestas = 0;
    list_map(vertice->edges, print_vertice, print_arestas);
    printf("\n");
    *print_arestas = 1;
  }
  else{
    printf("%2d ", vertice->id);
  }
}


void muda_grau_vertice(void *_vertice, void* _modifier){
  struct Vertice *vertice = (struct Vertice *) _vertice;
  int *modifier = _modifier;
  vertice->degree += *modifier;
}

bool is_vertice_equal(void *_in_list, void *_out_list){
  struct Vertice *in_list = (struct Vertice*) _in_list;
  int *out_list = (int *) _out_list;
  if (in_list->id == *out_list)
    return true;
  return false;
}




Graph graph_create(){
  struct Graph* graph = (struct Graph*) malloc(sizeof(struct Graph));
  graph->number_of_vertices = 0;
  graph->vertices = list_create(is_vertice_equal);
  return graph;
}


// Função privada (kkk, "privada")
struct Vertice* create_vertice(int id, int k){
  struct Vertice* vertice = (struct Vertice*) malloc(sizeof(struct Vertice));
  vertice->id = id;
  vertice->edges = list_create(is_vertice_equal);
  vertice->degree = 0;
  vertice->active = 1;
  if (id < k) vertice->color = id;
  else vertice->color = -1;

  // printf("Criando vertice %d, com cor %d\n", vertice->id, vertice->color);
  return vertice;
}

Vertice graph_insert_vertice(Graph _graph, int id, int k){
  struct Graph *graph = (struct Graph*) _graph;
  struct Vertice* vertice = create_vertice(id, k);

  graph->number_of_vertices += 1;
  list_insertEnd(graph->vertices, vertice);

  return vertice;
}


bool check_if_edge_exists(struct Vertice *vertice1, struct Vertice *vertice2){
  Posic posic = list_searchElement(vertice1->edges, vertice2);
  if (posic == NULL) return false;
  return true;
}
void graph_insert_edge(Graph _graph, int id_src, Vertice _vertice_src, int id_dest, Vertice _vertice_dest){
  struct Graph *graph = (struct Graph *) _graph;

  // Get vertice src
  struct Vertice *vertice_src;
  if (id_src != -1){
    Posic posic = list_searchElement(graph->vertices, &id_src);
    vertice_src = list_getContent(graph->vertices, posic);
  }
  else{
    vertice_src = _vertice_src;
  }

  // Get vertice dst
  struct Vertice *vertice_dst;
  if(id_dest != -1){
    Posic posic = list_searchElement(graph->vertices, &id_dest);
    vertice_dst = list_getContent(graph->vertices, posic);
  }
  else{
    vertice_dst = _vertice_dest;
  }

  // check if edge exists
  if (check_if_edge_exists(vertice_src, vertice_dst)) return;


  vertice_src->degree += 1;
  vertice_dst->degree += 1;

  list_insertEnd(vertice_src->edges, vertice_dst);
  list_insertEnd(vertice_dst->edges, vertice_src);
}


int graph_deactivate(Graph _graph, int id, Vertice _vertice){
  struct Graph *graph = (struct Graph *) _graph;

  struct Vertice *vertice;
  if (id != -1){
    Posic posic = list_searchElement(graph->vertices, &id);
    vertice = list_getContent(graph->vertices, posic);
  }
  else{
    vertice = _vertice;
  }

  // Diminuir o grau de todos os vértices
  int modifier = -1;
  list_map(vertice->edges, muda_grau_vertice, &modifier);

  // Diminuir o número de vertices no grafo
  graph->number_of_vertices -= 1;
  vertice->active = 0;

  return vertice->id;
}

int graph_activate(Graph _graph, int id, Vertice _vertice){
  struct Graph *graph = (struct Graph *) _graph;

  struct Vertice *vertice;
  if (id != -1){
    Posic posic = list_searchElement(graph->vertices, &id);
    vertice = list_getContent(graph->vertices, posic);
  }
  else{
    vertice = _vertice;
  }

  vertice->active = 1;


  // Aumentar o grau de todos os vertices
  int modifier = 1;
  list_map(vertice->edges, muda_grau_vertice, &modifier);
  

  // Aumentar a quantidade de vertices no grafo
  graph->number_of_vertices += 1;

  return vertice->id;
}

void get_max_degree(void *_vertice, void *_max_k){
  struct Vertice *vertice = (struct Vertice *) _vertice;
  struct Argument *max_k = (struct Argument *) _max_k;

  if (vertice->id < max_k->k_inicial || vertice->active == 0) return;
  

  if(max_k->vertice == NULL)
    max_k->vertice = vertice;
  else{
    // Se o grau for igual
    if(max_k->vertice->degree == vertice->degree){
      // Troca se o novo id for menor
      if(max_k->vertice->id > vertice->id)
        max_k->vertice = vertice;
    }
    else{
      // Se o grau for maior
      if(vertice->degree > max_k->vertice->degree)
        max_k->vertice = vertice;
    }
  }

}
Vertice graph_get_max_degree(Graph _graph, int k, int k_inicial){
  struct Graph *graph = (struct Graph *) _graph;

  struct Argument *max_k = (struct Argument *) malloc(sizeof(struct Argument));
  max_k->vertice = NULL;
  max_k->k = k;
  max_k->k_inicial = k_inicial;

  list_map(graph->vertices, get_max_degree, max_k);
  struct Vertice *vertice = max_k->vertice;
  free(max_k);
  return vertice;
}


void get_min_degree(void *_vertice, void *_min_k){
  struct Vertice *vertice = (struct Vertice *) _vertice;
  struct Argument *min_k = (struct Argument *) _min_k;
  // if(min_k->k == 4)
    // printf("\nID: %d, grau: %d, k: %d active: %d", vertice->id, vertice->degree, min_k->k, vertice->active);

  if(vertice->id < min_k->k_inicial || vertice->active == 0 || vertice->degree >= min_k->k){
    // printf("ID do vertice: %d, grau: %d, k: %d\n", vertice->id, vertice->degree, min_k->k);
    return;
  }

  if(min_k->vertice == NULL)
    min_k->vertice = vertice;
  else{
    // Se o grau for igual
    if(min_k->vertice->degree == vertice->degree){
      // Troca se o novo id for menor
      if(min_k->vertice->id > vertice->id)
        min_k->vertice = vertice;
    }

    else{
      // Se o grau for menor
      if(vertice->degree < min_k->vertice->degree)
        min_k->vertice = vertice;
    }
  }

}
Vertice graph_get_min_degree(Graph _graph, int k, int k_inicial){
  struct Graph *graph = (struct Graph *) _graph;

  struct Argument *min_k = (struct Argument *) malloc(sizeof(struct Argument));
  min_k->vertice = NULL;
  min_k->k = k;
  min_k->k_inicial = k_inicial;

  list_map(graph->vertices, get_min_degree, min_k);

  struct Vertice* vertice = min_k->vertice;

  free(min_k);
  return vertice;
}

Vertice graph_check_if_exists(Graph _graph, int id){
  struct Graph *graph = (struct Graph *) _graph;
  Posic posic = list_searchElement(graph->vertices, &id);
  if(posic == NULL)
    return posic;

  Vertice vertice = list_getContent(graph->vertices, posic);
  return vertice;
}

void graph_print(Graph _graph){
  struct Graph *graph = (struct Graph *) _graph;
  int *i = (int*) malloc(sizeof(int));
  *i = 1;
  list_map(graph->vertices, print_vertice, i);
  free(i);

}


int graph_get_vertice_degree(Vertice _vertice){
  struct Vertice *vertice = (struct Vertice *) _vertice;
  return vertice->degree;
}

int graph_get_vertice_id(Vertice _vertice){
  struct Vertice *vertice = (struct Vertice *) _vertice;
  return vertice->id;
}

int graph_get_number_of_vertices(Graph _graph){
  struct Graph *graph = (struct Graph *) _graph;
  return graph->number_of_vertices;
}


void get_colors(Vertice _vertice, void *args){
  struct Vertice *vertice = (struct Vertice *) _vertice;
  bool *colors = (bool *) args;

  if (vertice->color != -1)
    colors[vertice->color] = true;
}
void graph_set_color_vertice(Vertice _vertice, int k){
  struct Vertice *vertice = (struct Vertice *) _vertice;

  // Inicializando vetor colors
  bool colors[k];
  int i;
  for (i = 0; i < k; i++)
    colors[i] = false;
  
  list_map(vertice->edges, get_colors, colors);

  i = 0;
  while (i < k){
    if (colors[i] == false) {
      vertice->color = i;
      break;
    }
    i++;
  }
}

int graph_get_color_vertice(Vertice _vertice){
  struct Vertice *vertice = (struct Vertice *) _vertice;
  return vertice->color;
}

void reset_vertice(Vertice _vertice, void *_k){
  struct Vertice *vertice = (struct Vertice *) _vertice;
  int *k = _k;
  if (vertice->id < *k)
    vertice->color = vertice->id;
  else
    vertice->color = -1;
}
void reset_graph(Graph _graph, int k_inicial){
  struct Graph *graph = (struct Graph *) _graph;
  list_map(graph->vertices, reset_vertice, &k_inicial);

}