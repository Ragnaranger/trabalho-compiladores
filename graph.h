#ifndef __GRAPH_H__
#define __GRAPH_H__

#include <stdbool.h>

typedef void * Content;
typedef void * Vertice;
typedef void * Graph;

Graph graph_create();

/**
 * In case you need to compare two vertices.
 */
bool is_vertice_equal(void *_in_list, void *_out_list);

/**
 * Creates and inserts a new vertice.
 * Returns pointer to the vertice.
 */
Vertice graph_insert_vertice(Graph _graph, int id, int k);


/**
 * Inserts an edge.
 * The first vertice can be identified with either it's pointer
 *   or by it's id. To choose, send either NULL or -1 to the other
 * The second has to be identified with it's id.
 */
void graph_insert_edge(Graph _graph, int id_src, Vertice _vertice_src, int id_dest, Vertice _vertice_dest);


/**
 * Removes an edge.
 * The edge can be identified either by it's pointer or it's id.
 * Returns the id of the edge.
 */
// int graph_remove(Graph _graph, int id, Vertice _vertice);

/**
 * Returns the id of the vertice with max degree.
 * If it's a draw, the smallest id will be chosen
 */
Vertice graph_get_max_degree(Graph _graph, int k, int k_inicial);


/**
 * Returns the id of the vertice with min degree.
 * If it's a draw, the smallest id will be chosen
 */
Vertice graph_get_min_degree(Graph _graph, int k, int k_inicial);

Vertice graph_get_vertice_by_content();

Vertice graph_check_if_exists(Graph _graph, int id);

void graph_print(Graph _graph);

int graph_get_vertice_degree(Vertice _vertice);

int graph_get_vertice_id(Vertice _vertice);

int graph_get_number_of_vertices(Graph _graph);

int graph_deactivate(Graph _graph, int id, Vertice _vertice);

int graph_activate(Graph _graph, int id, Vertice _vertice);

void graph_set_color_vertice(Vertice vertice, int k);

int graph_get_color_vertice(Vertice vertice);

void reset_graph(Graph _graph, int k);

#endif