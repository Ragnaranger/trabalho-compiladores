#include <stdio.h>
#include <string.h>

#include "graph.h"
#include "stdlista.h"
#include "regalloc.h"


int main(){
  char linha[10];

  // Primeira linha: nome do grafo
  scanf("%[^\n]",linha);
  scanf("\n");

  int nome_grafo;
  sscanf(linha, "%*s %d", &nome_grafo);

  // Segunda linha: quantidade de k
  scanf("%[^\n]",linha);
  scanf("\n");

  int k_inicial;
  sscanf(linha, "K=%d", &k_inicial);

  int k = k_inicial;
  
  Graph graph = graph_create();

  printf("Graph %d -> Physical Registers: %2d\n----------------------------------------\n----------------------------------------\n"
            , nome_grafo, k);
  printf("K = %d\n", k);

  build_graph(graph, k);
  // graph_print(graph);

  ListStart pilha = list_create(is_vertice_equal);
  int k_em_que_deu_spill = 1;
  simplify(graph, pilha, k, k_inicial);
  assign(graph, pilha, k, &k_em_que_deu_spill, k_inicial);
  
  k--;

  while(k > 1) {
    reset_graph(graph, k_inicial);
    printf("\n----------------------------------------\nK = %d\n", k);
    // graph_print(graph);

    simplify(graph, pilha, k, k_inicial);
    assign(graph, pilha, k, &k_em_que_deu_spill, k_inicial);
    k--;
  }

  printf("\n----------------------------------------\n----------------------------------------");

  while (k_inicial > 1){
    printf("\nGraph %d -> K = %2d: ", nome_grafo, k_inicial);
    if (k_inicial > k_em_que_deu_spill)
      printf("Successful Allocation");
    else
      printf("SPILL");
    k_inicial --;
  }

  return 0;

}