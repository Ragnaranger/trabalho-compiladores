#include "regalloc.h"
#include "stdlista.h"


void build_graph(Graph graph, int k){
  char linha[5000];

  // Enquanto a entrada não terminar
  while(scanf("%[^\n]",linha) != EOF){

    // printf("%s\n", linha);
    // Primeiro item é o novo vértice
    int id;
    char *ids;
    ids = strtok(linha, " ");
    id = atoi(ids);

    // Ignorando o segundo argumento
    ids = strtok(NULL, " ");
    ids = strtok(NULL, " ");

    Vertice vertice = graph_check_if_exists(graph, id);
    if (vertice == NULL)
      vertice = graph_insert_vertice(graph, id, k);

    // Rodar até o final da linha
    while(ids != NULL){

      id = atoi(ids);
      // sscanf(linha, "%d", &id);
      Vertice vertice2 =  graph_check_if_exists(graph, id);

      if (vertice2 == NULL){
        vertice2 = graph_insert_vertice(graph, id, k);
      }

      graph_insert_edge(graph, -1, vertice, -1, vertice2);
      
      ids = strtok(NULL, " ");
    }

    scanf("\n");
  }
}

void simplify(Graph graph, ListStart pilha, int k, int k_inicial){
  bool potential_spill = false;
  while(graph_get_number_of_vertices(graph) > 0){
    Vertice vertice = graph_get_min_degree(graph, k, k_inicial);

    if (vertice == NULL){
      // printf("Vertice == NULL ou degree > k");
      vertice = graph_get_max_degree(graph, k, k_inicial);
      potential_spill = true;
    }
    

    // Acabaram os vertices maiores que k
    if (vertice == NULL){
      // printf("\nACABARAM OS VERTICES\n");
      break;
    }

    printf("\nPush: %2d", graph_get_vertice_id(vertice));
    if (potential_spill){
      printf(" *");
    }
    // printf("\n");

    list_insertEnd(pilha, vertice);
    graph_deactivate(graph, -1, vertice);
    potential_spill = false;
  }

}

void assign(Graph graph, ListStart pilha, int k, int *spill, int k_inicial){
  Vertice vertice;
  while (list_getLen(pilha) > 0){
    vertice = list_pop(pilha);
    graph_activate(graph, -1, vertice);
    graph_set_color_vertice(vertice, k);
    int color = graph_get_color_vertice(vertice);

    printf("\nPop: %2d -> ", graph_get_vertice_id(vertice));

    if(color == -1){
      printf("NO COLOR AVAILABLE");
      if (*spill < k)
        *spill = k;

      while (list_getLen(pilha) > 0) {
        vertice = list_pop(pilha);
        graph_activate(graph, -1, vertice);
      }
      break;
    }
    else
      printf("%d", color);
  }

}