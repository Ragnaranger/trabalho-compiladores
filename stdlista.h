#ifndef __STDLISTA_H__
#define __STDLISTA_H__
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdarg.h>


typedef void* ListStart;

typedef void* ListNode;


/**
 * - Anything you want to store in a list
 * - The list will store the pointer and return it
 * when the content of a node is asked
 */
typedef void* Item;

/**
 *  Position of a list node.
 *  Used to reach specific positions of list faster
 *  Every time you insert an item on a list, the funcion will
    return a posic.
 */
typedef void* Posic;

typedef bool (*isEqualFunction)(void* element_in_list, void* element_out_of_list);

/**
 * Function that receives an element and an argument defined by the user
 */
typedef void (*mapFunction)(void* content, void* args);


typedef bool (*bool_function)(void* content, void* args);


/**
 * Free an item from memory
 */
typedef void (*freeItemFunction)(Item item);


ListStart list_create(isEqualFunction isEqual);

Posic list_insertEnd( ListStart _list, Item item );

/**
 * If posic is NULL the element will be inserted at the end of the list
 */
Posic list_insertBefore( ListStart _list, Item item, Posic posic );

/**
 * If posic is NULL the element will be inserted at the end of the list
 */
// Não pronto
// Posic list_insertAfter( ListStart _list, Item item, Posic _posic );

/**
 * Returns the posic of the node that contains the item
 */
Posic list_searchElement( ListStart _list, Item item );


/**
 * returns the element
 */
Item list_removeElement(ListStart _list, Posic _posic);

/**
 * returns the element
 */
Item list_removeFirst(ListStart _list);

/**
 * Returns the content of given posic
 */
Item list_getContent(ListStart _list, Posic _posic);


// ListNode list_getElement( ListStart _list, ListNode posic );

Item list_getFirst( ListStart _list );

Item list_getLast( ListStart lista );

// ListNode list_getNext( ListStart _list, ListNode* posic );

// ListNode list_getPrevious( ListStart lista, ListNode* posic );

/**
 * Frees the list and all inside
 * The function parameter is a function that frees your item
 * 
 */
void list_deleteList( ListStart _list, freeItemFunction freeItem );

int list_getLen( ListStart _list);

void list_map(ListStart _list, mapFunction function, void* args);

ListStart list_generate_sublist(ListStart list, bool_function function, void* args);

Item list_pop(ListStart _list);

#endif