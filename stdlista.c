#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdarg.h>

#include "stdlista.h"


struct __ListNode{
    struct __ListNode* prev;
    struct __ListNode* next;
    void* content;
};


struct __ListStart{
    int size;
    isEqualFunction isEqual;
    struct __ListNode* firstElement;
    struct __ListNode* lastElement;
};

ListStart list_create(isEqualFunction isEqual){

   struct __ListStart* list = (struct __ListStart*) malloc( sizeof(struct __ListStart) );
   list->firstElement = NULL;
   list->lastElement = NULL;
   list->size = 0;
   list->isEqual = isEqual;

   return list;

}

Posic list_insertEnd( ListStart _list, void* element ){
    struct __ListStart* list = (struct __ListStart*) _list;
    
    struct __ListNode* node = (struct __ListNode*) malloc(sizeof(struct __ListNode));

    if(list->size == 0){
        list->firstElement = list->lastElement = node;
        node->prev = node->next = NULL;
    }
    else{
        list->lastElement->next = node;
        node->prev = list->lastElement;
        node->next = NULL;
        list->lastElement = node;
    }

    list->size++;
    node->content = element;
    return node;

}

Posic list_insertBefore( ListStart _list, void* element, Posic _posic ){
    //Adicionar o caso de ser o primeiro elemento
    if(_posic == NULL)
        return list_insertEnd(_list, element);
    

    struct __ListNode* posic = (struct __ListNode*) _posic;
    struct __ListStart* list = (struct __ListStart*) _list;

    struct __ListNode* node  = (struct __ListNode*) malloc( sizeof(struct __ListNode) );

    if( list->firstElement == posic ){
        list->firstElement = node;
    }

    node->next = posic;
    node->prev = posic->prev; // Pode ser NULL
    posic->prev->next = node;
    posic->prev = node;

    list->size++;
    node->content = element;

    return node;
    
}

Posic list_searchElement( ListStart _list, void* element ){

    struct __ListStart* list = (struct __ListStart*) _list;

    struct __ListNode* posic = (struct __ListNode*) list->firstElement;

    if (posic == NULL) return posic;

    while( !(list->isEqual)(posic->content, element) ){
        posic = posic->next;
        if (posic == NULL) break;
    }

    return posic;

}

Item list_removeElement(ListStart _list, Posic _posic){
    struct __ListStart* list = (struct __ListStart*) _list;
    struct __ListNode* posic = (struct __ListNode*) _posic;

    if(posic->next == NULL){
        // Posic é o último
        list->lastElement = posic->prev;

    }
    else{
        posic->next->prev = posic->prev;
    }

    if(posic->prev == NULL){
        // Posic é o primeiro
        list->firstElement = posic->next;
    }
    else{
        posic->prev->next = posic->next;
    }

    list->size--;

    void* content = posic->content;
    free(posic);
    return content;
    

}


Item list_removeFirst(ListStart _list){

    struct __ListStart* list = (struct __ListStart*) _list;

    if(list->size == 0){
        printf("The list is empty. @removeFirst function\n");
        return NULL;
    }

    struct __ListNode* node = list->firstElement;

    if(node->next == NULL){
        list->lastElement = NULL;
        list->firstElement = NULL;
    }
    else{
        node->next->prev = NULL;
        list->firstElement = node->next;
    }

    void* content = node->content;
    list->size--;

    free(node);
    return content;

}

Item list_getContent(ListStart _list, Posic _posic){
    struct __ListNode* posic = (struct __ListNode*) _posic;
    return posic->content;
}


Item list_getFirst( ListStart _list ){

    struct __ListStart* list = (struct __ListStart*) _list;

    return list->firstElement->content;

}

Item list_getLast( ListStart _list ){

    struct __ListStart* list = (struct __ListStart*) _list;

    return list->lastElement->content;
}


void list_deleteList( ListStart _list, freeItemFunction freeItem ){
    struct __ListStart* lista = (struct __ListStart *) _list;
    struct __ListNode* node;
    struct __ListNode* next_node;

    node = lista->firstElement;
    while (node != NULL){

        freeItem(node->content);

        next_node = node->next;
        free(node);

        node = next_node;
    }
    
    free(lista);
}

int list_getLen( ListStart _list){
    struct __ListStart* lista = (struct __ListStart *) _list;
    return lista->size;
}

void list_map(ListStart _list, mapFunction function, void* args){
    struct __ListStart* lista = (struct __ListStart *) _list;
    struct __ListNode* node = lista->firstElement;
    while (node != NULL){
        function(node->content, args);
        node = node->next;
    }
}


ListStart list_generate_sublist(ListStart _list, bool_function function, void* args){
    struct __ListStart *lista = (struct __ListStart *) _list;
    ListStart nova_lista = list_create(lista->isEqual);

    struct __ListNode *node = lista->firstElement;
    while(node != NULL){
        if( function(node->content, args) )
            list_insertEnd(nova_lista, node->content);

        node = node->next;
    }

    return nova_lista;
}

Item list_pop(ListStart _list){
    struct __ListStart *lista = (struct __ListStart *) _list;
    Item item = list_removeElement(lista, lista->lastElement);
    return item;
}