#ifndef __REGALLOC_H__
#define __REGALLOC_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "graph.h"
#include "stdlista.h"

void build_graph(Graph graph, int k);

void simplify(Graph graph, ListStart pilha, int k, int k_inicial);

void assign(Graph graph, ListStart pilha, int k, int *spil, int k_inicial);

#endif